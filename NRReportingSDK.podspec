#
# Be sure to run `pod lib lint NRReportingSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NRReportingSDK'
  s.version          = '1.1.0'
s.summary          = 'NextRadio Reporting SDK automates much of the TagStation reporting process with a few lines of code.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  The NextRadio Reporting SDK automates much of the TagStation reporting process by managing device registration, location retrieval, and listening session reports. The SDK is lightweight and requires just a few lines of code to integrate with your existing application.
                       DESC

  s.homepage         = 'https://code4121@bitbucket.org/bonneville/nextradiosdkpod.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Cody Nelson' => 'cnelson@bonneville.com' }
  s.source           = { :http => 'https://code4121@bitbucket.org/bonneville/nextradiosdkpod.git' }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'NRReportingSDK/Classes/**/*'
  
  # s.resource_bundles = {
  #   'NRReportingSDK' => ['NRReportingSDK/Assets/*.png']
  # }

  s.public_header_files = 'NRReportingSDK/Classes/**/*.h'
  s.frameworks = 'UIKit', 'Foundation'
  # s.dependency 'AFNetworking', '~> 2.3'
end
