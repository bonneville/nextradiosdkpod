# NRReportingSDK

[![CI Status](http://img.shields.io/travis/Cody Nelson/NRReportingSDK.svg?style=flat)](https://travis-ci.org/Cody Nelson/NRReportingSDK)
[![Version](https://img.shields.io/cocoapods/v/NRReportingSDK.svg?style=flat)](http://cocoapods.org/pods/NRReportingSDK)
[![License](https://img.shields.io/cocoapods/l/NRReportingSDK.svg?style=flat)](http://cocoapods.org/pods/NRReportingSDK)
[![Platform](https://img.shields.io/cocoapods/p/NRReportingSDK.svg?style=flat)](http://cocoapods.org/pods/NRReportingSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NRReportingSDK is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NRReportingSDK'
```

## Author

Cody Nelson, cnelson@bonneville.com

## License

NRReportingSDK is available under the MIT license. See the LICENSE file for more info.
