//
//  Created by John Koszarek on 12/18/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation

/// Sends JSON data to a TagStation service endpoint.
final class ReportingGateway {

    // HTTP request type
    static let putRequest = "PUT"

    // HTTP header - Content-Type
    static let contentTypeHeader = "Content-Type"
    static let contentTypeValue = "application/json"

    // HTTP header - Accept
    static let acceptHeader = "Accept"
    static let acceptValue = "application/json"

    // Query string parameter - TagStation device ID
    static let tagStationDeviceIdParameter = "tsd="

    // Query string parameter - SDK session (transaction) ID
    static let sdkSessionIdParameter = "sdkSessionId="

    // True if we should use the TagStation development environment, false if we should use the
    // production environment.
    var isDevelopmentEnvironment = false

    /**
     Sends JSON data to TagStation Analytics.
     */
    func dataTask(serviceEndpoint: String,
                  sessionId: SessionId,
                  jsonData: Data,
                  tagStationDeviceId: TagStationDeviceId,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        var urlComponents = URLComponents(string: serviceEndpoint)!
        urlComponents.percentEncodedQuery = urlRequestProperties(sessionId: sessionId,
                                                                 tagStationDeviceId: tagStationDeviceId)

        if isDevelopmentEnvironment {
            print("URL request: \(urlComponents.url!)")
        }

        var request = URLRequest(url: urlComponents.url!,
                                 cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy,
                                 timeoutInterval: NetworkConstants.requestTimeoutInterval)
        request.setValue(ReportingGateway.contentTypeValue, forHTTPHeaderField: ReportingGateway.contentTypeHeader)
        request.setValue(ReportingGateway.acceptValue, forHTTPHeaderField: ReportingGateway.acceptHeader)
        request.httpMethod = ReportingGateway.putRequest
        request.httpBody = jsonData

        let session = URLSession.shared
        let sessionDataTask = session.dataTask(with: request, completionHandler: completionHandler)
        sessionDataTask.resume()
    }

    /**
     Returnes a URL query string containing a session ID and a TagStation device ID.
     */
    private func urlRequestProperties(sessionId: SessionId, tagStationDeviceId: TagStationDeviceId) -> String {
        var queryStringParameter = ReportingGateway.tagStationDeviceIdParameter
        queryStringParameter.append(tagStationDeviceId.id!.addingURIPercentEncoding()!)
        queryStringParameter.append("&")
        queryStringParameter.append(ReportingGateway.sdkSessionIdParameter)
        queryStringParameter.append(sessionId.id)
        return queryStringParameter
    }

    /**
     Returns the session ID value from the supplied URLResponse.
     */
    class func sdkSessionIdValue(response: URLResponse?) -> String? {
        if let httpResponse = response as? HTTPURLResponse {
            if let url = httpResponse.url {
                return sdkSessionIdValue(url: url)
            }
        }
        return nil
    }

    /**
     Returns the session ID value from the supplied URL.
     */
    private class func sdkSessionIdValue(url: URL) -> String? {
        let urlComponents = URLComponents(string: url.absoluteString)
        if let queryItems = urlComponents?.queryItems {
            let parameterSubstring = String(
                    ReportingGateway.sdkSessionIdParameter.prefix(
                    ReportingGateway.sdkSessionIdParameter.count - 1))
            for queryItem in queryItems {
                if queryItem.name == parameterSubstring {
                    return queryItem.value
                }
            }
        }
        return nil
    }
}
