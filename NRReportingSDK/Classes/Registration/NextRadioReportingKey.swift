//  Created by John Koszarek on 1/16/18.
//  Copyright © 2018 NextRadio. All rights reserved.

import Foundation

/// The name of a host media app as it identifies itself to TagStation, and is used when registering
/// a device with the TagStation's analytics endpoint. TagStation internally refers to the reporting
/// key as the "client name".
/// NextRadioReportingKey objects are serializable.
public final class NextRadioReportingKey: NSObject, NSCoding, Archivable
{
    static let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let archiveUrl = documentsDirectory.appendingPathComponent("tsaNextradioReportingKey")

    public enum NextRadioReportingKeyError: Error {
        case unableToArchive
        case unableToDelete
    }

    public internal(set) var key: String?

    public override init() {
        super.init()
    }

    public init(key: String) {
        super.init()
        self.key = key
    }

    // MARK: NSCoding protocol

    /**
     Returns an object initialized from data in a given unarchiver.
     */
    public required convenience init?(coder aDecoder: NSCoder) {
        guard let decodedKey = aDecoder.decodeObject(forKey: "nrReportingKey") as? String else {
            return nil
        }
        self.init(key: decodedKey)
    }

    /**
     Encodes the receiver using a given archiver.
     */
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(key, forKey: "nrReportingKey")
    }

    // MARK: Archivable protocol

    public func saveObject() throws {
        let success = NSKeyedArchiver.archiveRootObject(self, toFile: NextRadioReportingKey.archiveUrl.path)
        if !success {
            throw NextRadioReportingKeyError.unableToArchive
        }
    }

    public class func loadObject() -> Any? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: NextRadioReportingKey.archiveUrl.path)
    }

    public class func deleteObject() throws {
        let exists = FileManager().fileExists(atPath: NextRadioReportingKey.archiveUrl.path)
        if exists {
            do {
                try FileManager().removeItem(atPath: NextRadioReportingKey.archiveUrl.path)
            }
            catch {
                throw NextRadioReportingKeyError.unableToDelete
            }
        }
    }
}
