//  Created by John Koszarek on 11/30/17.
//  Copyright © 2017 NextRadio. All rights reserved.

import Foundation

///  Called during device registration.
@objc
public protocol DeviceRegistrationDelegate {

    /**
     Called when a TagStation device ID is obtained during device registration.
     */
    func didReceiveTagStationDeviceId(tagStationDeviceId: TagStationDeviceId)
}
