//  Created by John Koszarek on 10/27/17.
//  Copyright © 2017 NextRadio. All rights reserved.

import Foundation

///  A unique ID used to identify a device.
///  A TagStation device ID is obtained when a device is registered, and is used when sending data to
///  TagStation's analytics endpoint.
///  TagStationDeviceId objects are serializable.
public final class TagStationDeviceId: NSObject, NSCoding, Archivable
{
    static let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let archiveUrl = documentsDirectory.appendingPathComponent("tsaTagstationDeviceId")

    public enum TagStationDeviceIdError: Error {
        case unableToArchive
        case unableToDelete
    }

    public internal(set) var id: String?

    public override init() {
        super.init()
    }

    public init(id: String) {
        super.init()
        self.id = id
    }

    // MARK: NSCoding protocol

     /**
      Returns an object initialized from data in a given unarchiver.
      */
    public required convenience init?(coder aDecoder: NSCoder) {
        guard let decodedId = aDecoder.decodeObject(forKey: "idKey") as? String else {
            return nil
        }
        self.init(id: decodedId)
    }

     /**
      Encodes the receiver using a given archiver.
      */
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "idKey")
    }

    // MARK: Archivable protocol

    public func saveObject() throws {
        let success = NSKeyedArchiver.archiveRootObject(self, toFile: TagStationDeviceId.archiveUrl.path)
        if !success {
            throw TagStationDeviceIdError.unableToArchive
        }
    }

    public class func loadObject() -> Any? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: TagStationDeviceId.archiveUrl.path)
    }

    public class func deleteObject() throws {
        let exists = FileManager().fileExists(atPath: TagStationDeviceId.archiveUrl.path)
        if exists {
            do {
                try FileManager().removeItem(atPath: TagStationDeviceId.archiveUrl.path)
            }
            catch {
                throw TagStationDeviceIdError.unableToDelete
            }
        }
    }
}
