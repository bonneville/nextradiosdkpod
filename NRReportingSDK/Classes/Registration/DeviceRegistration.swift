//
//  Registers a device, such as an iPhone.
//  When a device is registered, the TagStation database will return a TagStation device ID that the
//  device should store locally.
//  A device needs to be registered before analytics event data can be sent to TagStation.
//
//  Created by John Koszarek on 11/21/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation

public final class DeviceRegistration {

    // Device registration service endpoints
    static let endpoint = "https://api.tagstation.com/sdk/v1.0/registration"
    static let endpointDev = "https://dev-api.tagstation.com/sdk/v1.0/registration"

    // HTTP request types
    static let postRequest = "POST" // use for first time registration
    static let putRequest = "PUT"   // use for re-registration

    // HTTP header - Content-Type
    static let contentTypeHeader = "Content-Type"
    static let contentTypeValue = "application/json"

    // HTTP header - Accept
    static let acceptHeader = "Accept"
    static let acceptValue = "application/json"

    // Query string parameter - TagStation device ID
    static let tagStationDeviceIdParameter = "tsd="

    // JSON class encapsulating the registration data sent to/received from TagStation.
    static let jsonObjectName = "data"

    // JSON attibute of the TagStation device ID that is returned.
    static let tagStationDeviceIdAttribute = "tsd"

    /** Called during device registration, such as when a TagStation device ID is received. */
    var delegate: DeviceRegistrationDelegate?

    // Status of the last attempt to register a device (i.e., calling registerDevice()).
    var deviceRegistrationStatus = DeviceRegistrationState.none

    // True if we should use the TagStation development environment, false if we should use the
    // production environment.
    var isDevelopmentEnvironment = false

    // Retruns the device registration service endpoint to use based on whether we're running in a
    // production or development environment.
    var serviceEndpoint: String {
        get {
            if isDevelopmentEnvironment {
                return DeviceRegistration.endpointDev
            }
            else {
                return DeviceRegistration.endpoint
            }
        }
    }

    /**
     Registers a device.
     May be used for first time registrations or for re-registrations.
     */
    public func registerDevice(registrationData: DeviceRegistrationData, tagStationDeviceId: TagStationDeviceId?) {
        if isDevelopmentEnvironment {
            if let data = registrationData.jsonAsData(enclosingObjectName: DeviceRegistration.jsonObjectName) {
                if let text = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as String? {
                    print("Device registration data: \(String(describing: text))")
                }
            }
        }

        let requestProperties = urlRequestProperties(tagStationDeviceId: tagStationDeviceId)

        var urlComponents = URLComponents(string: serviceEndpoint)!
        urlComponents.percentEncodedQuery = requestProperties.queryString

        var request = URLRequest(url: urlComponents.url!,
                                 cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy,
                                 timeoutInterval: NetworkConstants.requestTimeoutInterval)
        request.setValue(DeviceRegistration.contentTypeValue, forHTTPHeaderField: DeviceRegistration.contentTypeHeader)
        request.setValue(DeviceRegistration.acceptValue, forHTTPHeaderField: DeviceRegistration.acceptHeader)
        request.httpMethod = requestProperties.httpMethod
        request.httpBody = registrationData.jsonAsData(enclosingObjectName: DeviceRegistration.jsonObjectName)

        let session = URLSession.shared
        let sessionDataTask = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            if response != nil {
                if self.isDevelopmentEnvironment {
                    print("Response: \(response.debugDescription)")
                }
            }
            if error != nil {
                self.deviceRegistrationStatus = .networkError
                if self.isDevelopmentEnvironment {
                    print("Device registration failed: \(error.debugDescription)")
                }
            }
            if data != nil {
                self.didReceiveResponse(data: data!)
            }
        })
        sessionDataTask.resume()
    }

    /**
     Returns the HTTP method and query string used when registering a device.
     The query string may be nil if this the first time a device is being registered.
     */
    private func urlRequestProperties(tagStationDeviceId: TagStationDeviceId?) ->
        (httpMethod: String, queryString: String?) {
        if tagStationDeviceId != nil {
            // Re-register a device. Use HTTP PUT, and return a URL query string containing a
            // TagStation device ID.
            var queryStringParameter = DeviceRegistration.tagStationDeviceIdParameter
            queryStringParameter.append(tagStationDeviceId!.id!.addingURIPercentEncoding()!)
            return (DeviceRegistration.putRequest, queryStringParameter)
        }
        else {
            // First time device registration. Use HTTP POST and no query string (we don't have
            // a TagStation device ID yet).
            return (DeviceRegistration.postRequest, nil)
        }
    }

    /**
     Extracts a TagStation device ID from the supplied JSON and creates a TagStationDeviceId that
     can be used when sending data to TagStation's analytics endpoint.
     */
    private func didReceiveResponse(data: Data) {
        if isDevelopmentEnvironment {
            if let text = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as String? {
                print("Response data: \(String(describing: text))")
            }
        }

        do {
            let responseJson = try JSONSerialization.jsonObject(with: data) as! NSDictionary
            let jsonObject = responseJson[DeviceRegistration.jsonObjectName] as! NSDictionary
            if let tsdValue = jsonObject[DeviceRegistration.tagStationDeviceIdAttribute] {
                if let tsdString = tsdValue as? String {
                    self.deviceRegistrationStatus = .success
                    let tagStationDeviceId = TagStationDeviceId(id: tsdString)
                    delegate?.didReceiveTagStationDeviceId(tagStationDeviceId: tagStationDeviceId)
                }
                else {
                    self.deviceRegistrationStatus = .jsonParseError
                }
            }
            else {
                self.deviceRegistrationStatus = .missingTsd
            }
        }
        catch {
            self.deviceRegistrationStatus = .jsonParseError
        }
    }
}
