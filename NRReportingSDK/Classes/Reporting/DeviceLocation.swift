//
//  Created by John Koszarek on 12/7/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation
import CoreLocation
import os.log

///  Business logic for tracking a device's location.
final class DeviceLocation: NSObject, CLLocationManagerDelegate {

    static let sharedInstance = DeviceLocation()

    /** The device's current location. */
    var location: CLLocation?

    /** True if location services have been authorized, false if not. */
    var locationServiceIsAuthorized: Bool {
        let authorized = CLLocationManager.authorizationStatus()
        if (authorized == .authorizedWhenInUse) || (authorized == .authorizedAlways) {
            return true
        }
        else {
            return false
        }
    }

    // Delivers location-related events.
    private var locationManager = CLLocationManager()

    // True if a request has been made to location services for continual updates, false if not.
    private var locationUpdatesStarted = false

    /**
     Creates a new object.
     */
    private override init() {
        super.init()
        locationManager.delegate = self
    }

    deinit {
        locationManager.delegate = nil
        if locationUpdatesStarted {
            stopUpdatingLocation()
        }
    }

    /**
     Starts the generation of updates that report the user’s current location.
     Also sets the location manager's distance filter to "kCLDistanceFilterNone" and its desired
     accuracy to "kCLLocationAccuracyBest".
     */
    func startUpdatingLocation() {
        locationManager.distanceFilter = kCLDistanceFilterNone      // default for iOS; making sure it is set
        locationManager.desiredAccuracy = kCLLocationAccuracyBest   // I think this is the default for iOS; making sure it is set
        locationManager.startUpdatingLocation()
        locationUpdatesStarted = true
    }

    /**
     Stops the generation of updates that report the user’s current location.
     */
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
        location = nil
        locationUpdatesStarted = false
    }

    /**
     Requests the one-time delivery of the user’s current location.
     */
    func requestLocation() {
        locationManager.requestLocation()
    }

    // MARK: TagStation Location data type creation

    /**
     Returns the device's location as a NextRadio data type.
     May return nil if the device's location is not available.
     */
    func makeLocation() -> Location? {
        var tsLocation: Location? = nil
        if let location = location {
            tsLocation = DeviceLocation.makeLocation(location: location)
        }
        return tsLocation
    }

    /**
     Returns the supplied location as a NextRadio data type.
     */
    class func makeLocation(location: CLLocation) -> Location {
        let creationTime = Date()
        let tsLocation = Location(creationTime: creationTime,
                                  latitude: location.coordinate.latitude,
                                  longitude: location.coordinate.longitude,
                                  gpsUtcTime: location.timestamp,
                                  deviceSpeed: location.speed,
                                  horizontalAccuracy: location.horizontalAccuracy,
                                  ipAddress: nil)
        return tsLocation
    }

    // MARK: CLLocationManagerDelegate protocol

    /** Tells this delegate that new location data is available. */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.last

        //print("Device latitude = \(location!.coordinate.latitude)")
        //print("Device longitude = \(location!.coordinate.longitude)")
    }

    /** Tells this delegate that the location manager was unable to retrieve a location value. */
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        os_log("Unable to get device location from CLLocationManager", log: OSLog.default, type: .error)
    }

    /**
     Tells the delegate that the authorization status for the application changed.
     Start updating location if the location manager's authorization status changes to:
     - .authorizedAlways
     - .authorizedWhenInUse
     Change this object's location property to nil and stop updating location if the location manager's
     authorization status changes to:
     - .notDetermined
     - .restricted
     - .denied
     */
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization: CLAuthorizationStatus) {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways:
            os_log("CLLocationManager authorization status: .authorizedAlways", log: OSLog.default, type: .info)
            if !locationUpdatesStarted {
                startUpdatingLocation()
            }
        case .notDetermined:
            os_log("CLLocationManager authorization status: .notDetermined", log: OSLog.default, type: .info)
            location = nil
            if locationUpdatesStarted {
                stopUpdatingLocation()
            }
        case .authorizedWhenInUse:
            os_log("CLLocationManager authorization status: .authorizedWhenInUse", log: OSLog.default, type: .info)
            if !locationUpdatesStarted {
                startUpdatingLocation()
            }
        case .restricted:
            os_log("CLLocationManager authorization status: .restricted", log: OSLog.default, type: .info)
            location = nil
            if locationUpdatesStarted {
                stopUpdatingLocation()
            }
        case .denied:
            os_log("CLLocationManager authorization status: .denied", log: OSLog.default, type: .info)
            location = nil
            if locationUpdatesStarted {
                stopUpdatingLocation()
            }
        }
    }
}
