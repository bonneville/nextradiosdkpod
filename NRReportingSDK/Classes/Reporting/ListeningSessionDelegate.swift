//
//  Created by John Koszarek on 1/10/18.
//  Copyright © 2018 NextRadio. All rights reserved.
//

import Foundation

/// Called to inform an observer of listening session state.
@objc
public protocol ListeningSessionDelegate {

    /**
     Called when a listening session started.

     - Parameter sessionListeningChannel: The listening session that was started.
     - Parameter impressionRadioEvent: The radio event received.
     - Parameter shouldReportData: If true, the method implementation should also send the event data to
       TagStation
     */
    func listeningSessionDidStart(sessionListeningChannel: SessionListeningChannel,
                                  impressionRadioEvent: ImpressionRadioEvent?,
                                  shouldReportData: Bool)

    /**
     Called when a listening session ended.

     - Parameter sessionListeningChannel: The listening session that was ended.
     - Parameter shouldReportData: If true, the method implementation should also send the event data to
       TagStation
     */
    func listeningSessionDidEnd(sessionListeningChannel: SessionListeningChannel, shouldReportData: Bool)

    /**
     Called when a radio event was received and it didn't start a new listening session, but we still want
     to sent the event data to TagStation.
     Note this will be a common occurrence. For instance, a radio event might have been received indicating
     that a new song was playing on the same session. This would not start/end a session, but we still want
     to report the radio event.

     - Parameter impressionRadioEvent: The radio event received.
     - Parameter shouldReportData: If true, the method implementation should also send the event data to
       TagStation.
     */
    func radioEventWasReceived(impressionRadioEvent: ImpressionRadioEvent, shouldReportData: Bool)
}
