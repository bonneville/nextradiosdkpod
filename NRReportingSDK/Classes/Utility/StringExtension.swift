//
//  Extension to the String class for URI percent encoding.
//  See: https://en.wikipedia.org/wiki/Percent-encoding
//
//  Created by John Koszarek on 11/6/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation

extension String {

    func addingURIPercentEncoding() -> String? {
        let unreservedCharacters = "-_.~"
        let unreservedAlphanumericCharacters = NSMutableCharacterSet.alphanumeric()
        unreservedAlphanumericCharacters.addCharacters(in: unreservedCharacters)

        var encoded = addingPercentEncoding(withAllowedCharacters:unreservedAlphanumericCharacters as CharacterSet)
        encoded = encoded?.replacingOccurrences(of: " ", with: "+")

        return encoded
    }
}
