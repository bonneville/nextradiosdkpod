//
//  Constants used during device registration and analytics/reporting.
//
//  Created by John Koszarek on 11/17/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation
import CoreLocation

public enum DeviceRegistrationState {
    case success
    case none           // no attempt to register has been made yet
    case networkError   // URLSession returned an error
    case jsonParseError
    case missingTsd     // no TagStation device ID in response
}

struct FrameworkConstants {
    /**
     Time between SDK heartbeats.
     */
    static let heartbeatTime = 120.0    // 2 minutes

    /**
     TagStation Analytics SDK version, formatted as MAJOR.MINOR.PATCH.BUILD
     For the first version of the SDK this will be hardcoded.
     */
    static let sdkVersion: String = "1.0.0.0"
}

struct DataIntakeConstants {
    /**
     TagStation Analytics DataIntake REST API JSON metadata version attribute name.
     */
    static let metadataVersionAttribute: String = "version"

    /**
     TagStation Analytics DataIntake REST API metadata version value name.
     */
    static let metadataVersionValue: Int = 1

    /**
     TagStation Analytics DataIntake REST API JSON object name for metadata.
     */
    static let metaJsonObjectName = "meta"

    /**
     TagStation Analytics DataIntake REST API JSON object name for enclosing data such as
     radio event impressions, location reporting, and listening sessions.
     */
    static let dataJsonObjectName = "data"
}

struct NetworkConstants {
    /**
     HTTP status code sent back from TagStation when data has been successfully sent.
     */
    static let successResponseCode = 204

    /**
     The timeout in seconds used when connecting to a TagStation endpoint. This is the timeout
     value used across NextRadio apps (Android and iOS).
     */
    static let requestTimeoutInterval = 10.0
}
