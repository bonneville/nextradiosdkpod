//
//  Created by John Koszarek on 2/14/18.
//  Copyright © 2018 NextRadio. All rights reserved.
//

import Foundation

/// Compares a SessionListeningChannel object to a ImpressionRadioEvent object.
/// Retruns true if a radio event should start a new listening session.
func shouldRadioEventStartNewListeningSession(lhs: ImpressionRadioEvent,
                                              rhs: SessionListeningChannel?) -> Bool {
    if rhs == nil {
        return true // if there's no listening session to compare with, start a new listening session
    }
    if lhs.deliveryType != rhs!.deliveryType
        || lhs.frequencyHz != rhs!.frequencyHz
        || lhs.frequencySubchannel != rhs!.frequencySubchannel {
        return true // if fields are not equal, start a new listening session
    }
    return false    // if fields are equal, don't start a new listening session
}

/// Compares two ImpressionRadioEvent objects.
/// Returns true if a radio event should start a new listening session.
func shouldRadioEventStartNewListeningSession(lhs: ImpressionRadioEvent,
                                              rhs: ImpressionRadioEvent?) -> Bool {
    if rhs == nil {
        return true // if there's no radio event to compare with, start a new listening session
    }
    if lhs.deliveryType != rhs!.deliveryType
        || lhs.frequencyHz != rhs!.frequencyHz
        || lhs.frequencySubchannel != rhs!.frequencySubchannel {
        return true // if fields are not equal, start a new listening session
    }
    return false    // if fields are equal, don't start a new listening session
}

/// Returns true if one listening session equals another.
/// Note we are not comparing all fields, just the fields that NextRadio and TagStation care about
/// as being the same.
func listeningSessionsAreEqual(lhs: SessionListeningChannel,
                               rhs: SessionListeningChannel) -> Bool {
    if lhs.deliveryType != rhs.deliveryType
        || lhs.frequencyHz != rhs.frequencyHz
        || lhs.frequencySubchannel != rhs.frequencySubchannel {
        return false
    }
    return true
}

/// Returns true if one radio events equals another.
/// Note we are not comparing all fields, just the fields that NextRadio and TagStation care about
/// as being the same.
func radioEventsAreEqual(lhs: ImpressionRadioEvent, rhs: ImpressionRadioEvent) -> Bool {
    if shouldRadioEventStartNewListeningSession(lhs: lhs, rhs: rhs) {
        return false
    }
    if lhs.artist != rhs.artist || lhs.title != rhs.title {
        return false
    }
    return true
}
