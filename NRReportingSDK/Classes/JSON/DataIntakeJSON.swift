//
//  Created by John Koszarek on 12/29/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation
import os.log

/// Creates JSON confirming to the format expected by the DataIntake REST API.
class DataIntakeJSON {

    /**
     The TagStation Analytics DataIntake REST API JSON metadata.
     */
    static let metaJsonObject = [DataIntakeConstants.metadataVersionAttribute : DataIntakeConstants.metadataVersionValue]

    /**
     The TagStation Analytics DataIntake REST API data.
     */
    private var jsonObjects = [String : Any]()

    /**
     Creates a new DataIntakeJSONConversion object using the supplied Array.
     */
    init(array: [NextRadioDataType]) {
        jsonObjects[DataIntakeConstants.metaJsonObjectName] = DataIntakeJSON.metaJsonObject

        var tagStationDictionaries = [Any]()
        for tagStationData in array {
            let dictionary = tagStationData.toDictionary()
            tagStationDictionaries.append(dictionary)
        }
        jsonObjects[DataIntakeConstants.dataJsonObjectName] = tagStationDictionaries
    }

    /**
     Creates a new DataIntakeJSONConversion object using the supplied Dictionary.
     */
    init(dictionary: [String : Any]) {
        jsonObjects[DataIntakeConstants.metaJsonObjectName] = DataIntakeJSON.metaJsonObject
        jsonObjects[DataIntakeConstants.dataJsonObjectName] = [dictionary]
    }

    /**
     Converts this object's data to the JSON format expected by the DataIntake REST API.

     - Returns: JSON formatted for use with the DataIntake REST API.
     */
    func asData() -> Data? {
        guard JSONSerialization.isValidJSONObject(jsonObjects) else {
            os_log("Invalid JSON", log: OSLog.default, type: .error)
            return nil
        }

        guard let newData = try? JSONSerialization.data(withJSONObject: jsonObjects) else {
            os_log("JSON serialization error", log: OSLog.default, type: .error)
            return nil
        }

        return newData
    }
}
