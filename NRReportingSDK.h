//
//  NRReportingSDK.h
//  NRReportingSDK
//
//  Created by John Koszarek on 2/5/18.
//  Copyright © 2018 NextRadio. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NRReportingSDK.
FOUNDATION_EXPORT double NRReportingSDKVersionNumber;

//! Project version string for NRReportingSDK.
FOUNDATION_EXPORT const unsigned char NRReportingSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NRReportingSDK/PublicHeader.h>


    
